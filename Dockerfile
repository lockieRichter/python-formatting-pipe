FROM python:3.6-slim

WORKDIR /app/

COPY pipe.py requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

ENTRYPOINT ["python3", "/app/pipe.py"]
