# Bitbucket Pipelines Pipe: python-formatting-pipe

Formats a repository of python code using autoflake, isort and black to a line length of 120 characters.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml

- pipe: docker://lockierichter/python-formatting-pipe
  variables:
    LINE_LENGTH: <integer>

```

## Variables

Variable | Usage
--- | ---
LINE_LENGTH | Define the length of the line to use when formatting with black. Default: 120.

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know.

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce
