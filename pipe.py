import os
import subprocess

from bitbucket_pipes_toolkit import Pipe

variables = {
    "LINE_LENGTH": {"type": "integer", "required": False, "default": 120},
}

pipe = Pipe(schema=variables)

if "BITBUCKET_CLONE_DIR" in os.environ:
    src_dir = os.environ["BITBUCKET_CLONE_DIR"]
else:
    src_dir = "."

LINE_LENGTH = pipe.get_variable("LINE_LENGTH")

try:
    subprocess.check_call(
        ["autoflake", "-r", f"{src_dir}", "--in-place", "--remove-all-unused-imports", "--exclude=__init__.py"]
    )
except Exception:
    pipe.fail(message="Failed to format code using autoflake.")
else:
    pipe.success(message="Successfully formated code using autoflake.")

try:
    subprocess.check_call(["isort", "-rc", f"{src_dir}"])
except Exception:
    pipe.fail(message="Failed to format code using isort.")
else:
    pipe.success(message="Successfully formated code using isort.")

try:
    subprocess.check_call(["black", f"{src_dir}", f"--line-length={LINE_LENGTH}"])
except Exception:
    pipe.fail(message="Failed to format code using black.")
else:
    pipe.success(message=f"Successfully formated code using black at a line length of {LINE_LENGTH}.")


pipe.success(message="Success formating code.")
